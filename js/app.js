$(document).ready(function (){

    let nbParts = $('.nb-parts input').val();
    let type = 0;
    let pate = 0;
    let extra = 0;
    let extra_tot = 0;
    let temp;

    $('.pizza-type label').hover(

        function () {
            $(this).find(".description").show();
        },
        function () {
            $(this).find(".description").hide();
        }
    )

    $('.nb-parts input').on('keyup', function () {

        const pizza = $('<span class="pizza-pict"></span>');
        nbParts = +$(this).val();

        $(".pizza-pict").remove();

        for(let i = 0; i<nbParts/6 ; i++){

            $(this).after(pizza.clone().addClass("pizza-6"));
        }

        if(nbParts%6 !== 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-' + nbParts%6);
        price();
    })

    $('.next-step').click(function () {
        $('.infos-client').slideDown();
        $(this).hide();
    })

    const input = $('<br><input type="text"/>');

    $('.add').click(function () {
        $(this).before(input.clone());
    })

    $('.done').click(function () {

        const name = $('#name').val();
        const thanks = $('<span>Merci ' + name + ' ! Votre commande sera livrée dans 15 minutes</span>');
        $('.main').slideUp();
        $('.headline').append(thanks);
    })

    $('input:radio[name="type"]').change(
        function(){
            if ($(this).is(':checked')) {
                type = $(this).data('price');
                price();
            }
        });

    $('input:radio[name="pate"]').change(
        function(){
            if ($(this).is(':checked')) {
                pate = $(this).data('price');
                price();
            }
        });

    $('input:checkbox[name="extra"]').change(
        function () {
            if ($(this).is(':checked')) {
                extra_tot += $(this).data('price');
            } else {
                extra_tot -= $(this).data('price');
            }
            extra = extra_tot;
            price();
        }
    );

    function price() {
        let price = type + pate + extra;
        price = (price/6)*nbParts;
        price = price.toFixed(2);
        $('#total').text(price+' €');
    }

    $.ajax({
        url: "https://api.openweathermap.org/data/2.5/weather?q=paris&appid=768a35a09a1701be84498950a95e7cf5",
        dataType: 'jsonp',
        success: function(results){
            console.log(results);
            temp = results.main.temp -273.15;
            temp = temp.toFixed(2);
            if(temp < 10){
                $('#livraison').append("<br/><b>La température extérieure à Paris étant de " + temp + "°C un supplément de 5 euros est ajouté à la livraison.</b>");
                $('#total-livraison').show();
            }
        }
    });
});